import mongoConnect from './mongo'
import config from '../configs'

/**
 * Connect to MongoDB or MySQL database
 */
async function dbConnect(): Promise<void> {
  if(config.env.DB_TYPE === 'mongodb') await mongoConnect()
}

export default dbConnect