import express from 'express'
const router = express.Router()

// Add Controllers & Validators
import Controller from '../../controllers/authentication'
router.route('/register').post(Controller.register)
router.route('/login').post(Controller.login)
export default router