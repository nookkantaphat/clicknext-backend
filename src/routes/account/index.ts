import express from 'express'
const router = express.Router()
import Controller from '../../controllers/account'
import bodyParser from 'body-parser'
const jsonParser = bodyParser.json()
router.get('/list',jsonParser,Controller.list)

export default router