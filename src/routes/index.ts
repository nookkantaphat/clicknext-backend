import {  Router } from 'express'
const router: Router = Router()

// ------ Add JWT to chosen routes
// import jwt    from 'express-jwt'
// import config from '../configs/config'
// const JwtCheck = jwt({ secret: config.jwt.key })
// router.use('/v1/samples', JwtCheck, sampleRouter)

// Sample APIs
import authenticationRoute from './authentication'
import lineRoute from './line'
import accountRoute from './account'
router.use('/authentication', authenticationRoute)
router.use('/line', lineRoute)
router.use('/account', accountRoute)


export default router