import express from 'express'
const router = express.Router()
// import { middleware } from '@line/bot-sdk'
const middleware = require('@line/bot-sdk').middleware
import config from '../../configs'
import Controller from '../../controllers/line'
// import { QueryData } from '../models/mongo_base'
import bodyParser from 'body-parser'
const jsonParser = bodyParser.json()
const { LINE_CHANNEL_SECRET, LINE_CHANNEL_ACCESS_TOKEN } = config.env
const configs = {
    channelAccessToken: `${LINE_CHANNEL_ACCESS_TOKEN}`,
    channelSecret: `${LINE_CHANNEL_SECRET}`
}
router.post('/webhook',middleware(configs),Controller.webhook)
// router.post('/webhook',jsonParser,Controller.webhook)
router.get('/message',jsonParser,Controller.accountMessage)
router.post('/send-message',jsonParser,Controller.sendMessage)
export default router