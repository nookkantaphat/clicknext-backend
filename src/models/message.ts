import mongoose from 'mongoose'
// import Errors   from 'http-errors'
import { BaseDocument, BaseModel, SchemaDefinition } from './mongo_base'

// -----------------------------------------------------------------------------------
// ------------------------------ Your Sample Interface ------------------------------
// -----------------------------------------------------------------------------------
export interface Message extends BaseDocument {
  message_type: string
  message_text: string
  message_timestamp: number
  user_type: string
  from_user_id: string
  to_user_id: string
  line_data?: object
}

// -----------------------------------------------------------------------------------
// ------------------------ Write Your Custom Methods in Model -----------------------
// -----------------------------------------------------------------------------------

declare module './mongo_base' {
  //   interface BaseModel<T> {
  //     // Add new methods to class ...
  //     greetings: (sampleId: string) => Promise<string>
  //     findByAge: (age: number) => Promise<Sample>
  //   }
}

/** Find Model & Greet by Name */
// BaseModel.prototype.greetings = async function(sampleId: string): Promise<string> {
//   const sample: Sample | null = await this.model.findById(sampleId)
//   console.log('sample: ', sample)
//   if(!sample) throw new Errors.NotFound(MESSAGES.MODEL_NOT_FOUND)
//   return 'Hi ' + sample.name + '!!'
// }

// /** Find Model By Age */
// BaseModel.prototype.findByAge = async function(username: string,password: string): Promise<Acccount> {
//   const account: Acccount | null = await this.model.findOne({ username,password })
//   if(!account) throw new Errors.Unauthorized()
//   return account
// }

// -----------------------------------------------------------------------------------
// ---------------------- Your MongoDB Schema Model Definition -----------------------
// -----------------------------------------------------------------------------------
const definition: SchemaDefinition = {
  message_type: { type: mongoose.Schema.Types.String, required: true, unique: false },
  message_text: { type: mongoose.Schema.Types.String, required: true, unique: false },
  message_timestamp: { type: mongoose.Schema.Types.String, required: true, unique: false },
  user_type: { type: mongoose.Schema.Types.String, required: true, unique: false },
  from_user_id: { type: mongoose.Schema.Types.String, required: false, unique: false },
  to_user_id: { type: mongoose.Schema.Types.String, required: false, unique: false },
  line_data: { type: mongoose.Schema.Types.String, required: false, unique: false}

}
const baseModel = new BaseModel<Message>(definition, 'message')

export default baseModel