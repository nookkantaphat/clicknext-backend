import { Request, Response, NextFunction } from "express"


interface Error {
  statusCode: number | string
  status?: number | string
  code?: number | string
  message: string
  data?: { [key: string]: string | boolean | unknown }
  errors: 'Error'
}

function transformer(err: Error, req: Request, res: Response, next: NextFunction): void {
  
  const response = res.result ? {
    status: '',
    statusCode: res.statusCode,
    success: res.result ? true : false,
    result: res.result,
  } : {
    statusCode: err.statusCode || (err.status || (err.code || 500)),
    url: req.url,
    message: err.message || 'Server Internal error',
    errors: err.data || err.errors || null
  }

  if (typeof response.statusCode !== 'number' || response.statusCode > 600 || response.statusCode < 100) {
    response.status = response.statusCode.toString()
    response.statusCode = 500
    console.log(' ------- ResDec - STRING STATUS CODE:', err)
  } else delete response.status

  if (response.statusCode >= 500) console.log(' ------- ResDec - SERVER ERROR:', err)
  // if(response.message) response.message = res.t(response.message as MESSAGES, req.language)

  res.status(response.statusCode).json(response)
  next()
}

export default transformer