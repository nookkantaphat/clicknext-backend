import express from 'express'
import helmet from 'helmet'
// import http from 'http'
// import { Server } from 'socket.io'
import router from './routes'
import cors from 'cors'
// import bodyParser from 'body-parser'
const app: express.Application = express()

// app.use(express.urlencoded({ extended: true }))
// app.use(bodyParser.json())
app.use(helmet())
app.use(cors())


app.use('/api', router)

// ------ Add Response Transformer (& error handler) to system
import transformer from './middlewares/transformer'

app.use(transformer)

export default app