import { Request, Response, NextFunction } from 'express'
import Errors from 'http-errors'
// import * as Sample from '../models/sample-mysql'
import ModelMessage from '../models/message'
import ModelAccount from '../models/account'
import config from '../configs'
// import { QueryData } from '../models/mongo_base'
const { LINE_CHANNEL_ACCESS_TOKEN } = config.env

const exportResult = {

    // Create Sample
    async webhook(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const io = req.app.get('io')
            // const data = req.body
            // if (data.password !== data.confirm_password) {
            //     throw new Errors.BadRequest('Password not match')
            // }
            // delete data.confirm_password
            console.log('body', req.body)

            const event = req.body.events[0]
            if (event) console.log(event)
            if (event && event.type == 'message') {
                const account = await ModelAccount.model.findOne({
                    line_user_id: event.source.userId
                })
                if (!account) {
                    fetch(`https://api.line.me/v2/bot/profile/${event.source.userId}`, {
                        headers: {
                            "Authorization": `Bearer ${LINE_CHANNEL_ACCESS_TOKEN}`,
                            "Content-Type": "application/json"
                        },
                        method: 'GET',
                    })
                        .then(response => response.json())
                        .then(async (result) => {
                            if (result) {
                                const account = await ModelAccount.add({
                                    username: event.source.userId,
                                    firstname: result.displayName,
                                    lastname: result.displayName,
                                    user_type: 'USER',
                                    password: '1234',
                                    email: 'test@test.com',
                                    line_user_id: event.source.userId,
                                })
                                io.emit('update-account', { data: account })

                            }
                        })
                        .catch(error => console.log('error', error));


                }
                if (account && account.status == 0) {
                    fetch("https://api.line.me/v2/bot/message/push", {
                        headers: {
                            "Authorization": `Bearer ${LINE_CHANNEL_ACCESS_TOKEN}`,
                            "Content-Type": "application/json"
                        },
                        method: 'POST',
                        body: JSON.stringify({
                            "to": account.line_user_id,
                            "messages": [
                                {
                                    "type": "text",
                                    "text": "Account has been block"
                                }
                            ]
                        })
                    })
                        .then(response => response.json())
                        .then(result => console.log(result))
                        .catch(error => console.log('error', error));
                    throw new Errors.BadRequest('Account has been block')
                }
                const result = await ModelMessage.add({
                    message_type: event.type,
                    message_text: event.message.text,
                    message_timestamp: event.timestamp,
                    user_type: event.source.type,
                    from_user_id: event.source.userId,
                    to_user_id: null,
                    line_data: JSON.stringify(req.body)
                })
                res.result = (result as any)._doc
                io.emit('message', { data: res.result, account: account })
            } else if (event.type == 'follow') {
                const account = await ModelAccount.model.findOne({
                    line_user_id: event.source.userId
                })
                if (!account) {
                    fetch(`https://api.line.me/v2/bot/profile/${event.source.userId}`, {
                        headers: {
                            "Authorization": `Bearer ${LINE_CHANNEL_ACCESS_TOKEN}`,
                            "Content-Type": "application/json"
                        },
                        method: 'GET',
                    })
                        .then(response => response.json())
                        .then(async (result) => {
                            if (result) {
                                const account = await ModelAccount.add({
                                    username: event.source.userId,
                                    firstname: result.displayName,
                                    lastname: result.displayName,
                                    user_type: 'USER',
                                    password: '1234',
                                    email: 'test@test.com',
                                    line_user_id: event.source.userId,
                                })
                                io.emit('update-account', { data: account })

                            }
                        })
                        .catch(error => console.log('error', error));


                }
            } else {
                console.log('body', req.body)
                console.log('event', event)
            }
            next(res)
        } catch (err) { next(err) }
    },

    async accountMessage(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            const account = await ModelAccount.model.findOne({
                _id: req.query.user_id
            })
            if (!account) throw new Errors.BadRequest('Account not found')

            const message = await ModelMessage.model.find({
                $or: [
                    {
                        from_user_id: account.line_user_id,
                    },
                    {
                        to_user_id: account.line_user_id
                    }
                ]
            })

            res.result = { message: message, account: account }
            next(res)
        } catch (err) { next(err) }
    },

    async sendMessage(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
            console.log(req.body)
            const account = await ModelAccount.model.findOne({
                _id: req.body.user_id
            })
            if (!account) throw new Errors.BadRequest('Account not found')
            
            const result = await ModelMessage.add({
                message_type: 'message',
                message_text: req.body.message,
                message_timestamp: Math.floor(new Date().getTime()),
                user_type: 'admin',
                from_user_id: null,
                to_user_id: account.line_user_id,
                line_data: null
            })
            res.result = (result as any)
            const io = req.app.get('io')
            io.emit('message', { data: res.result, account: account })
            // fetch('https://api.line.me/v2/bot/message/push', {

            // })
            fetch("https://api.line.me/v2/bot/message/push", {
                headers: {
                    "Authorization": `Bearer ${LINE_CHANNEL_ACCESS_TOKEN}`,
                    "Content-Type": "application/json"
                },
                method: 'POST',
                body: JSON.stringify({
                    "to": result.to_user_id,
                    "messages": [
                        {
                            "type": "text",
                            "text": req.body.message
                        }
                    ]
                })
            })
                .then(response => response.json())
                .then(result => console.log(result))
                .catch(error => console.log('error', error));

            next(res)
        } catch (err) { next(err) }
    },


}

export default exportResult