export interface EnvironmentModel {
    readonly NODE_ENV : string
    readonly APP_ENV  : string
    readonly DB_HOST  : string
    readonly DB_USER? : string
    readonly DB_PASS? : string
    readonly DB_PORT  : number
    readonly DB_NAME  : string
    readonly DB_TYPE  : string
    readonly DB_CONNECTION: string
    readonly SERVER_PROTOCOL: string
    readonly SERVER_HOST : string
    readonly SERVER_PORT : number
    readonly LOGGER_HOST : string
    readonly LOGGER_PORT : number
    readonly REDIS_HOST? : string
    readonly REDIS_PORT? : number
    readonly REDIS_PASS? : string
    readonly ADMIN_USER? : string
    readonly ADMIN_PASS? : string
    readonly JWT_SECRET? : string
    readonly LINE_CHANNEL_SECRET? : string
    readonly LINE_CHANNEL_ACCESS_TOKEN?: string
  }

  export interface ConfigModel {
    // readonly jwt       : JwtModel
    readonly env       : EnvironmentModel
    // readonly baseURL   : string
    // readonly roleTypes : StringType
    readonly sortTypes : StringType
    // readonly MS        : MS_Configs
    // readonly regex     : RegexType
    readonly maxPageSizeLimit : number
  }

  export interface StringType {
    [key: string]: string
  }