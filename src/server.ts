import 'reflect-metadata'
import http from 'http'
import app from './app'
import config from './configs'
import dbConnect from './database'
import { Server } from 'socket.io'

const { NODE_ENV, SERVER_PROTOCOL, SERVER_HOST, SERVER_PORT } = config.env
// let server: http.Server = http.createServer(app)
const server = http.createServer(app)
const io = new Server(server, {
    cors: {
        origin: ["http://localhost:3300","http://localhost:3000","https://chat.kantaphat.com","https://chat-backend.kantaphat.com"],
        credentials: false
    }
});
app.set('io', io)
// ---------------- Start Server ----------------
async function startServer(server: http.Server): Promise<void> {
    server.listen(SERVER_PORT || 4000, () => {
        const url = `${SERVER_PROTOCOL || 'http'}://${SERVER_HOST || 'localhost'}:${SERVER_PORT || 4000}`
        console.log(`API is now running on ${url} in ${NODE_ENV || 'development'} mode`)
    })
}
(async () => {
    try {
        await dbConnect()
        await startServer(server)
    } catch (error) {
        throw Error(`>>>>> Server Connection Error: ${error}`)
    }
})()